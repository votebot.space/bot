

package space.votebot.bot.constants

import java.awt.Color

/**
 * Wrapper for [Discordapp.com/branding][https://discordapp.com/branding] colors and some other colors:
 */
@Suppress("KDocMissingDocumentation", "unused", "SpellcheckingInspection")
object Colors {
    // Discord
    val BLURLPLE: Color = Color(114, 137, 218)
    val FULL_WHITE: Color = Color(255, 255, 255)
    val GREYPLE: Color = Color(153, 170, 181)
    val DARK_BUT_NOT_BLACK: Color = Color(44, 47, 51)
    val NOT_QUITE_BLACK: Color = Color(33, 39, 42)
    // Other colors
    val LIGHT_RED: Color = Color(231, 76, 60)
    val DARK_RED: Color = Color(192, 57, 43)
    val LIGHT_GREEN: Color = Color(46, 204, 113)
    val DARK_GREEN: Color = Color(39, 174, 96)
    val BLUE: Color = Color(52, 152, 219)
    val YELLOW: Color = Color(241, 196, 15)
}

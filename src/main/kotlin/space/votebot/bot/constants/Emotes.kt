

package space.votebot.bot.constants

/**
 * Useful collection of Discord emotes.
 *
 *
 * Designed by [Rxsto#1337](https://rxsto.me)
 * Bot needs to be on [https://discord.gg/8phqcej](https://discord.gg/8phqcej)
 */
@Suppress("KDocMissingDocumentation", "unused")
object Emotes {
    const val LOADING: String = "<a:loading:547513249835384833>"
    const val ERROR: String = "<:error:535827110489620500>"
    const val WARN: String = "<:warn:535832532365737987>"
    const val INFO: String = "<:info:535828529573789696>"
    const val SUCCESS: String = "<:success:535827110552666112>"
}
